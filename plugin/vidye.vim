
nnoremap <silent> <plug>colorschemeNext :call vidye#changeColor(1)<cr>
nnoremap <silent> <plug>colorschemePrev :call vidye#changeColor(0)<cr>

function! s:setTestColorschemeMaps()
	nmap <right> <plug>colorschemeNext
	nmap <left>  <plug>colorschemePrev
	" TODO
	nnoremap <down> :put =vidye#getLastColo()<cr>
	" nnoremap <up> <plug>
endfunction

command! TestColorschemes call <sid>setTestColorschemeMaps()
