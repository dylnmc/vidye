
" change colorscheme
if !exists('s:lastColoInd')
	let s:lastColoInd = -1
	let s:ignoredColorInds = []
endif

function! vidye#changeColor(next)
	let l:colos = getcompletion('', 'color')
	let l:coloLen = len(l:colos)
	if a:next
		let s:lastColoInd += 1
		if s:lastColoInd >= l:coloLen
			let s:lastColoInd = 0
		endif
	else
		let s:lastColoInd -= 1
		if s:lastColoInd < 0
			let s:lastColoInd = l:coloLen - 1
		endif
	endif
	execute 'colorscheme '.l:colos[s:lastColoInd]
	redraw
	unsilent echon '('.s:lastColoInd.')colorscheme '.l:colos[s:lastColoInd]
endfunction

function! vidye#getLastColo()
	return getcompletion('', 'color')[s:lastColoInd]
endfunction

