![Vi Dye title](assets/vi-dye.png)

## Install and Setup

### Install

```diff
  call plug#begin($VIMDIR.'/plugged')
+ let g:plug_url_format = 'https://gitlab.com/%s.git'
+ Plug 'dylnmc/ViDye'
+ unlet g:plug_url_format
  call plug#end()
```

* Refer to your plugin manager's installation instructions to install, but note
  that this plugin is hosted on gitlab


## Setup

Little setup is require. By default nothing is mapped. If you want to map things
yourself, then the following are the defaults if you should run
`:TestColorschemes` (described below). More functionality soon for saving nice
colorschemes to a file with ease, ignoring ugly colorschemes, and linking
directly to the URL for the colorscheme

```vim
nmap <right> <plug>colorschemeNext
nmap <left> <plug>colorschemePrev
```

`:TestColorschemes` does exactly that which is mentioned above. It maps
<kbd>right</kbd> and <kbd>left</kbd> to `<plug>colorschemeNext` and
`<plug>colorschemePrev` (respectively).

`:TestColorschemes` also maps <kbd>down</kbd> to `:put =vidye#getLastColo()<cr>`
which puts the current colorscheme on the next line.

## Future

I plan to have a git-ignored file in the root of the project that is written to
when the user presses <kbd>down</kbd> or <kbd>up</kbd>. <kbd>up</kbd> will add
the current colorscheme to favourites, and <kbd>down</kbd> will add the current
colorscheme to the ignore list.

